# Compact Autocoli 

This repository is accompanying the manuscript for eLife: ["Autotrophic growth of E. coli is achieved by a small number of genetic changes"](https://elifesciences.org/reviewed-preprints/88793)

Roee Ben-Nissan*, Eliya Milshtein*, Vanessa Pahl, Benoit de Pins, Ghil Jona, Dikla Levi, Hadas Yung, Noga Nir, Dolev Ezra, Shmuel Gleizer, Hannes Link, Elad Noor, Ron Milo

-corresponding author: RM [ron.milo@weizmann.ac.il](url)

# Data 

All data is given in the folder [`data`](data)

# Figures

### jupyter notebook

* The Jupyter notebook called [`plot_figures.ipynb`](./plot_figures.ipynb) recreates figures 2B, 4B, 4D, 5 and 6C

* The Jupyter notebook called [`plot_supp_figures.ipynb`](./plot_supp_figures.ipynb) recreates supplamentry figures 1, 2, 6 and 7

### MATLAB

To generate the growth curves of figures 2 and 6 use the file `Bioteck_viewer.m` under the folder `MATLAB`
- Instructions for this GUI can be found in the file `BiotekViewer_5_SOP.docx` under the same folder

* Figure 2A is generated with the data file `DataS3-compact_vs_evolved_growth_experiment_DATA_45mM_5pctO2.xlsx` and map file `DataS4-compact_vs_evolved_growth_experiment_MAP_45mM_5pctO2.xlsx`

* Figure 6A is generated with the data files `DataS8-Growth_experiment_WT_vs_DM_FDH_variants_CO2_40mMFor_DATA.xlsx` and `DataS10-Growth_experiment_WT_vs_DM_FDH_variants_CO2_NoFormate_DATA.xlsx` and map files `DataS9-Growth_experiment_WT_vs_DM_FDH_variants_CO2_40mMFor_MAP.xlsx` and `DataS11-Growth_experiment_WT_vs_DM_FDH_variants_CO2_Noformate_MAP.xlsx`


To generate the growth rates barplots of figures 2 and 6 use the file `barplot_growth_rates.m` under the folder `MATLAB`

* Figure 2A is generated with the data file `___`

* Figure 6B is generated with the data files `___`

