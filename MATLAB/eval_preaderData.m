function eval_preaderData(a,b,c,d,e)
global preader
% define num of points --> regression over X minutes
pnts        = b;
smooth_pnts = a;
cutOD       = c;
cutT        = e;
% Number of data points
Dsize = size(preader.time,1);

h = waitbar(0,'Calculating Rates');

preader.time_2 =[];
preader.mue = [];
preader.mueMax = [];

preader.gfp1_od = [];
preader.pa1= [];
preader.pa1Max = [];
preader.gfp1Max = [];

preader.gfp2_od = [];
preader.pa2 = [];
preader.pa2Max = [];
preader.gfp2Max = [];


for i = 1:size(preader.labels,1)

   waitbar(i /size(preader.labels,1))
%% OD
    % Convert OD platereader -> real OD
    if(d==1)
        %Spark
        if i<97
        od = (preader.od(:,i) - mean(preader.od(1:3,i))+0.01)*3.15; 
        else
        od = (preader.od(:,i) - mean(preader.od(1:3,i))+0.01)*4.47;
        end
    elseif(d==2)
        if i<97
        %merged plates
            od = (preader.od(:,i) - mean(preader.od(1:3,i))+0.01)*3.15;
        else
            od = (preader.od(:,i) - mean(preader.od(1:3,i))+0.01)*3.15;
    
        end
    else
        %gcm
        if i<97
        od = (preader.od(:,i) - mean(preader.od(1:3,i))+0.01)*4.47;
        else
        od = (preader.od(:,i) - mean(preader.od(1:3,i))+0.01)*3.15;
        end
    end
    
    
    % Smooth data with moving average filter (smooth function) 
    preader.od_s(:,i) = smooth(od,smooth_pnts); 
    od_s = smooth(od,smooth_pnts); 
    
%% GFP1
    % GFP Data
    gfp1 = preader.gfp1(:,i);
    % Smooth data with moving average filter (smooth function) 
    preader.gfp1_s(:,i) = smooth(gfp1,smooth_pnts); 
    gfp1_s = smooth(gfp1,smooth_pnts);
%% GFP2
    % GFP Data
    gfp2 = preader.gfp2(:,i);
    % Smooth data with moving average filter (smooth function) 
    preader.gfp2_s(:,i) = smooth(gfp2,smooth_pnts); 
    gfp2_s = smooth(gfp2,smooth_pnts);
    
%% Growth rate und pa time
    preader.time_2 = preader.time(pnts:end-pnts); 
   
%% Growth Rate

    for k = pnts:size(preader.time,1)-pnts
        % x ans y values of regression
        y =  log(od_s([k-pnts+1:k+pnts])); 
        t =  (preader.time([k-pnts+1:k+pnts]));
        % regression function (see Matlab help)
        dummy = regress(y,[t ones(2*pnts,1)]);
        
        if(dummy(1)<0)
            dummy(1)=0;
        end
        
        preader.mue(k-pnts+1,i) = dummy(1);    
    end
%% Find MueMax
% Find all indexes above OD cut off
isAboveOD = find(od_s(pnts:end-pnts)>cutOD);

if (cutT>0)
isAboveOD = find(preader.time_2>cutT);
end

% Find maximum 
[mueMax  ixMueMax]   = max(preader.mue(isAboveOD,i));
mueMax_t  = preader.time_2(isAboveOD(ixMueMax));  

preader.mueMax(i) = mueMax;
preader.mueMax_t(i) = mueMax_t;
     
%% PA1: Promotor Activity (dGFP/dt/OD)

    for k = pnts:size(preader.time,1)-pnts
        % x ans y values of regression
        y =  (gfp1_s([k-pnts+1:k+pnts])); 
        t =  (preader.time([k-pnts+1:k+pnts]));
        % regression function (see Matlab help)
        dummy = regress(y,[t ones(2*pnts,1)]);
        %Slope
        dGFPdt = dummy(1);
        preader.pa1(k-pnts+1,i) = dGFPdt/od_s(k);
    end
    
%% Get PA1 at mueMax
preader.pa1Max(i) = preader.pa1(isAboveOD(ixMueMax),i);
preader.gfp1Max(i) = gfp1_s(isAboveOD(ixMueMax))/od_s(isAboveOD(ixMueMax));
%% PA2: Promotor Activity (dGFP/dt/OD)

    for k = pnts:size(preader.time,1)-pnts
        % x ans y values of regression
        y =  (gfp2_s([k-pnts+1:k+pnts])); 
        t =  (preader.time([k-pnts+1:k+pnts]));
        % regression function (see Matlab help)
        dummy = regress(y,[t ones(2*pnts,1)]);
        %Slope
        dGFPdt = dummy(1);
        preader.pa2(k-pnts+1,i) = dGFPdt/od_s(k);
    end

%% Get PA2 at mueMax
preader.pa2Max(i) = preader.pa2(isAboveOD(ixMueMax),i);   
preader.gfp2Max(i) = gfp2_s(isAboveOD(ixMueMax))/od_s(isAboveOD(ixMueMax)); 

%% Make specific GFP signal
preader.gfp1_od(:,i) = preader.gfp1(:,i)./preader.od(:,i);
preader.gfp1Max(i) = preader.gfp1_od(isAboveOD(ixMueMax),i); 

preader.gfp2_od(:,i) = preader.gfp2(:,i)./preader.od(:,i);
preader.gfp2Max(i) = preader.gfp2_od(isAboveOD(ixMueMax),i); 
end
 
 close(h) 
