function varargout = Biotek_Viewer(varargin)
% BIOTEK_VIEWER MATLAB code for Biotek_Viewer.fig
%      BIOTEK_VIEWER, by itself, creates a new BIOTEK_VIEWER or raises the existing
%      singleton*.
%
%      H = BIOTEK_VIEWER returns the handle to a new BIOTEK_VIEWER or the handle to
%      the existing singleton*.
%
%      BIOTEK_VIEWER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BIOTEK_VIEWER.M with the given input arguments.
%
%      BIOTEK_VIEWER('Property','Value',...) creates a new BIOTEK_VIEWER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Biotek_Viewer_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Biotek_Viewer_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Biotek_Viewer

% Last Modified by GUIDE v2.5 27-Jun-2017 09:57:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Biotek_Viewer_OpeningFcn, ...
                   'gui_OutputFcn',  @Biotek_Viewer_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargoutF}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Biotek_Viewer is made visible.
function Biotek_Viewer_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Biotek_Viewer (see VARARGIN)

% Choose default command line output for Biotek_Viewer
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Biotek_Viewer wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Biotek_Viewer_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clear global preader

global preader



%% Get excel data 
%Directory

[FileName,PathName,FilterIndex]=uigetfile('*.xlsx','Select biotek File)');
[status,sheets,~]= xlsfinfo([PathName FileName])

[indexS,~] = listdlg('ListString',sheets,'PromptString','Select OD sheet');

[num,txt,raw] = xlsread([PathName FileName],sheets{indexS});

% take the whole col with time vector
preader.time   = num(1:end,1)*24;
preader.od     = num(:,3:end);
% labels
preader.labels = raw(1,3:size(preader.od,2)+2)';

set(handles.uitable1,'Data',[preader.labels,preader.labels,num2cell(max(preader.od)')])

%% Read GFP Data 1

[indexS,~]      = listdlg('ListString',sheets,'PromptString','Select Fluro1 sheet');
[num,txt,raw]   = xlsread([PathName FileName],sheets{indexS});
preader.gfp1    = num(:,3:end);

%% Read GFP Data 1
[indexS,~]      = listdlg('ListString',sheets,'PromptString','Select Fluro2 sheet');
[num,txt,raw]   = xlsread([PathName FileName],sheets{indexS});
preader.gfp2    = num(:,3:end);


% Currate columns
preader.labels = raw(1,3:size(preader.od,2)+2)';

set(handles.uitable1,'Data',[preader.labels,preader.labels,num2cell(max(preader.od)')])
set(handles.listbox1,'Value',1)
% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA

global preader

preader.strainID = {};
preader.strainNames = {}; 
preader.sampleSize = [];
preader.smpIX =[];

% Get file directorty
[FileName,PathName,FilterIndex]=uigetfile('*.xlsx','Select Map File');
% Open xls data
[num,txt,raw] = xlsread([PathName FileName],'Sheet1');

for i = 1: size(preader.labels,1)
   ix = find(strcmp(preader.labels(i),txt(1:end,1)));
   preader.strainID(i,1) =  txt(ix,2);
end

emptyIX = find(strcmp(preader.strainID,'empty'));

% Remove empty columns
[a b] = find(isnan(preader.od));
preader.od(:,emptyIX) = [];
preader.labels(emptyIX) = [];
preader.gfp1(:,emptyIX) = [];
preader.gfp2(:,emptyIX) = [];
preader.strainID(emptyIX)= [];

preader.strainNames = unique(preader.strainID,'stable');
preader.sampleSize = size(preader.strainNames,1);

preader.smpIX = [];
for i = 1:preader.sampleSize
    preader.smpIX(i,:) = find(strcmp(preader.strainNames(i),preader.strainID))';
end

set(handles.uitable1,'Data',[preader.labels,preader.strainID, num2cell(max(preader.od)')])
set(handles.listbox1,'String',preader.strainNames)

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


prompt = {'Smoothing Points','Regression Points','OD cut-off','Time cut-off','Spark(1) /Infinite(2) /GCM(3)'};
dlg_title = 'Input';
num_lines = 1;
def = {'5','8','0','0','1'};
answer = inputdlg(prompt,dlg_title,num_lines,def);

a = str2num(answer{1});
b = str2num(answer{2});
c = str2num(answer{3});
e = str2num(answer{4});
d = str2num(answer{5});

eval_preaderData(a,b/2,c,d,e);
mean_preaderData;
set(handles.listbox1,'Value',1)

% --- Executes on button press in pushbutton4.% PLOT Growth
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
yyy=[0 1]
global preader
close(figure(1))
indexStr = get(handles.listbox1,'Value');
tsString = get(handles.edit1,'String');
% RGB colors for plot
C=[101/255 44/255 144/255;
    0 166/255 81/255;
    202/255 219/255 42/255;
    246/255 138/255 30/255;
    108/255 200/255 190/255;
    82/255 79/255 161/255;
    241/255 90/255 34/255;
    182/255 35/255 103/255;
    253/255 184/255 19/255;
    0 172/255 220/255;
    237/255 28/255 36/255];


for i = 1:size(indexStr,2)
    
    ix = indexStr(i);
    
    figure(1)
   
    subplot(4,1,1)
    [hl, hp] = boundedline(preader.time,preader.od_m(:,ix),(preader.od_std(:,ix)),'k-','alpha');
    plot(preader.time,preader.od_m(:,ix),'k-','Color',C(i,:),'LineWidth',2)
    set(gca,'XLim',str2num(tsString))
    xlabel('time [h]')
    ylabel('OD600')
    ylim([0 0.3]);
    xlim([0 200]);
    hold on 
    
    subplot(4,1,2)
    [hl, hp] = boundedline(preader.time,log(preader.od_m(:,ix)),(preader.od_std(:,ix)),'k-','alpha');
    plot(preader.time,log(preader.od_m(:,ix)),'k-','Color',C(i,:),'LineWidth',2)
    set(gca,'XLim',str2num(tsString))
    xlabel('time [h]')
    ylabel('logOD600)')
    ylim([-5 1]);
    xlim([0 200]);
    hold on    
    
    subplot(4,1,3)
    [hl, hp] = boundedline(preader.time_2,preader.mue_m(:,ix),preader.mue_std(:,ix),'k-','alpha');
    plot(preader.time_2,preader.mue_m(:,indexStr(i)),'k-','Color',C(i,:),'LineWidth',2)
    plot(ones(1,2)*preader.mueMaxT_m(indexStr(i)),[preader.mueMax_m(indexStr(i))-preader.mueMax_std(indexStr(i)) ...
    preader.mueMax_m(indexStr(i))+preader.mueMax_std(indexStr(i))] ,'k-','Color',C(i,:),'LineWidth',2)
    %plot(preader.mueMaxT_m(indexStr(i)),preader.mueMax_m(indexStr(i)),'ko','MarkerFaceColor',C(i,:),'MarkerSize',8)
    xlabel('time [h]')
    ylabel('growth rate [h^-^1]')    
    set(gca,'XLim',str2num(tsString))
    ylim([0 0.04]);
    xlim([0 200]);
    hold on
% 
%     subplot(6,1,4)
%     [hl, hp] = boundedline(preader.time,preader.gfp1_od_m(:,ix),preader.gfp1_od_std(:,ix),'k-','alpha');
%     plot(preader.time,preader.gfp1_od_m(:,ix),'k-','Color',C(i,:),'LineWidth',2)
%     xlabel('time [h]')
%     ylabel('fluorescense intensity \n /OD [AU]')    
%     ylim([0 8000]);
%     xlim([0 20]);
%     hold on
%     
%     subplot(6,1,5)
%     [hl, hp] = boundedline(preader.time_2,preader.pa1_m(:,ix),preader.pa1_std(:,ix),'k-','alpha');
%     plot(preader.time_2,preader.pa1_m(:,indexStr(i)),'k-','Color',C(i,:),'LineWidth',2)
%     xlabel('time [h]')
%     ylabel('promoter activity')
%     ylim([0 2500]);
%     xlim([0 20]);
%     hold on
%     
    subplot(6,1,6)
    plot(1,1,'k-','Color',C(i,:),'LineWidth',5)
    hold on

    %plot(preader.mueMaxT_m(indexStr(i)),preader.gfp1Max_m(indexStr(i)),'ko','MarkerFaceColor',C(i,:),'MarkerSize',8)
end

legend(preader.strainNames(indexStr),'Location','best')
x0=100;
y0=200;
width=1000;
height=260
set(gcf,'units','points','position',[x0,y0,width,height])



% --- Executes on button press in pushbutton5. PLOT PA1
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global preader
close(figure(2))
indexStr = get(handles.listbox1,'Value');
tsString = get(handles.edit1,'String');
% RGB colors for plot
C=[101/255 44/255 144/255;
    0 166/255 81/255;
    202/255 219/255 42/255;
    246/255 138/255 30/255;
    108/255 200/255 190/255;
    82/255 79/255 161/255;
    241/255 90/255 34/255;
    182/255 35/255 103/255;
    253/255 184/255 19/255;
    0 172/255 220/255;
    237/255 28/255 36/255];

for i = 1:size(indexStr,2)
    
    ix = indexStr(i);
    
    figure(2)
    subplot(3,1,1)
    [hl, hp] = boundedline(preader.time,preader.gfp1_od_m(:,ix),preader.gfp1_od_std(:,ix),'k-','alpha');
    plot(preader.time,preader.gfp1_od_m(:,ix),'k-','Color',C(i,:),'LineWidth',2)
    xlabel('time [h]')
    ylabel('fluorescense intensity/OD [AU]')    
    ylim([0 350000]);
    hold on
    %plot(preader.mueMaxT_m(indexStr(i)),preader.gfp1Max_m(indexStr(i)),'ko','MarkerFaceColor',C(i,:),'MarkerSize',8)
    set(gca,'XLim',str2num(tsString))
       
    subplot(3,1,2)
    [hl, hp] = boundedline(preader.time_2,preader.pa1_m(:,ix),preader.pa1_std(:,ix),'k-','alpha');
    plot(preader.time_2,preader.pa1_m(:,indexStr(i)),'k-','Color',C(i,:),'LineWidth',2)
    xlabel('time [h]')
    ylabel('promoter activity')
    ylim([0 350000]);
 

    hold on
    %plot(ones(1,2)*preader.mueMaxT_m(indexStr(i)),[preader.pa1Max_m(indexStr(i))-preader.pa1Max_std(indexStr(i)) ...
    %preader.pa1Max_m(indexStr(i))+preader.pa1Max_std(indexStr(i))] ,'k-','Color',C(i,:),'LineWidth',2)
    %plot(preader.mueMaxT_m(indexStr(i)),preader.pa1Max_m(indexStr(i)),'ko','MarkerFaceColor',C(i,:),'MarkerSize',8)
    set(gca,'XLim',str2num(tsString))
    
    
    subplot(3,1,3)
    plot(1,1,'k-','Color',C(i,:),'LineWidth',5)
    hold on
end

legend(preader.strainNames(indexStr),'Location','best')
x0=100;
y0=200;
width=1000;
height=260
set(gcf,'units','points','position',[x0,y0,width,height])

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
indexStr = get(handles.listbox1,'Value');
tsString = get(handles.edit1,'String');

global preader
close(figure(3))

% RGB colors for plot
C=[101/255 44/255 144/255;
    0 166/255 81/255;
    202/255 219/255 42/255;
    246/255 138/255 30/255;
    108/255 200/255 190/255;
    82/255 79/255 161/255;
    241/255 90/255 34/255;
    182/255 35/255 103/255;
    253/255 184/255 19/255;
    0 172/255 220/255;
    237/255 28/255 36/255];

for i = 1:size(indexStr,2)
    
    ix = indexStr(i);
    
    figure(3)
    subplot(1,3,1)
    [hl, hp] = boundedline(preader.time,preader.gfp2_m(:,ix),preader.gfp2_std(:,ix),'k-','alpha');
    plot(preader.time,preader.gfp2_m(:,ix),'k-','Color',C(i,:),'LineWidth',2)
    xlim([0 20])    
    hold on 
    set(gca,'XLim',str2num(tsString))
    xlim([0 20]) 
    subplot(1,3,2)
    [hl, hp] = boundedline(preader.time_2,preader.pa2_m(:,ix),preader.pa2_std(:,ix),'k-','alpha');
    plot(preader.time_2,preader.pa2_m(:,indexStr(i)),'k-','Color',C(i,:),'LineWidth',2) 
    xlim([0 20])
    ylim([0 2500])
    hold on
    plot(ones(1,2)*preader.mueMaxT_m(indexStr(i)),[preader.pa2Max_m(indexStr(i))-preader.pa2Max_std(indexStr(i)) ...
    preader.pa2Max_m(indexStr(i))+preader.pa2Max_std(indexStr(i))] ,'k-','Color',C(i,:),'LineWidth',2)
    xlim([0 20])    
    ylim([0 8000])
    plot(preader.mueMaxT_m(indexStr(i)),preader.pa2Max_m(indexStr(i)),'ko','MarkerFaceColor',C(i,:),'MarkerSize',8)
    xlim([0 20])
    ylim([0 8000])
    set(gca,'XLim',str2num(tsString))
    subplot(1,3,3)
    plot(1,1,'k-','Color',C(i,:),'LineWidth',5)
    xlim([0 20])
    ylim([0 2500])
    hold on
end

legend(preader.strainNames(indexStr),'Location','best')
x0=100;
y0=200;
width=1000;
height=260
set(gcf,'units','points','position',[x0,y0,width,height])

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global preader
% Get file directorty
[FileName,PathName,FilterIndex]=uiputfile('*.mat','Select File Name');
% Open xls data
save([PathName FileName],'preader')

% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global preader
[FileName,PathName,FilterIndex]=uigetfile('*.mat','Select File');
load([PathName FileName]);
set(handles.uitable1,'Data',[preader.labels,preader.strainID, num2cell(max(preader.od)')])
set(handles.listbox1,'String',preader.strainNames)
set(handles.listbox1,'Value',1)