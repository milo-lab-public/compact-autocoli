load('compact_paper_83vs66.mat')
figure (1)
subplot(1,1,1)
vals=[preader.mueMax_m(1,[8,11])];
errgr = [preader.mueMax_std(1,[8,11])];
labels = {['compact'],['evolved']};
% b= bar(vals,'grouped');
[ngroups,nbars] = size(vals);
ylabel('max growth rate [h^-^1]');
b= bar(vals);
hold on
errorbar(vals,errgr,'k','linestyle','none');
xticklabels(labels)
    